//
//  HCEventProperty.h
//  HCube-iOS-SDK
//
//  Created by Dmitriy Karachentsov on 3/10/16.
//  Copyright © 2016 Dmitriy Karachentsov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HCEventProperty : NSObject

@property (nonatomic, copy, nonnull) NSString *key;
@property (nonatomic, copy, nonnull) NSString *value;

- (nullable NSDictionary *)serializedObject;

@end
