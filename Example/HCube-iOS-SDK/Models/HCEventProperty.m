//
//  HCEventProperty.m
//  HCube-iOS-SDK
//
//  Created by Dmitriy Karachentsov on 3/10/16.
//  Copyright © 2016 Dmitriy Karachentsov. All rights reserved.
//

#import "HCEventProperty.h"

@implementation HCEventProperty

- (NSDictionary *)serializedObject {
    if (self.key.length == 0 || self.value.length == 0) {
        return nil;
    }
    return @{ self.key : self.value };
}

@end
