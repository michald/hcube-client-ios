//
//  main.m
//  HCube-iOS-SDK
//
//  Created by Dmitriy Karachentsov on 09/29/2016.
//  Copyright (c) 2016 Dmitriy Karachentsov. All rights reserved.
//

@import UIKit;
#import "HCAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([HCAppDelegate class]));
    }
}
