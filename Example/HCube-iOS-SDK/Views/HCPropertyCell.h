//
//  HCPropertyCell.h
//  HCube-iOS-SDK
//
//  Created by Dmitriy Karachentsov on 2/10/16.
//  Copyright © 2016 Dmitriy Karachentsov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HCPropertyCell;

@protocol HCPropertyCellDelegate <NSObject>

- (void)trackPropertyCell:(HCPropertyCell *)cell keyDidChanged:(NSString *)key;
- (void)trackPropertyCell:(HCPropertyCell *)cell valueDidChanged:(NSString *)value;

@end

@interface HCPropertyCell : UITableViewCell

@property (weak, nonatomic) IBOutlet id<HCPropertyCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet UITextField *keyField;
@property (weak, nonatomic) IBOutlet UITextField *valueField;

@end
