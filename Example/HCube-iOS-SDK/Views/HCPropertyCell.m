//
//  HCPropertyCell.m
//  HCube-iOS-SDK
//
//  Created by Dmitriy Karachentsov on 2/10/16.
//  Copyright © 2016 Dmitriy Karachentsov. All rights reserved.
//

#import "HCPropertyCell.h"

@interface HCPropertyCell () <UITextFieldDelegate>
@end

@implementation HCPropertyCell

#pragma mark - @IBActions

- (IBAction)keyChangedAction:(UITextField *)sender {
    [self.delegate trackPropertyCell:self keyDidChanged:sender.text];
}

- (IBAction)valueChangedAction:(UITextField *)sender {
    [self.delegate trackPropertyCell:self valueDidChanged:sender.text];
}

#pragma mark - <UITextFieldDelegate>

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (self.valueField == textField) {
        if (self.keyField.text.length == 0) {
            [self.keyField becomeFirstResponder];
            return NO;
        }
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (self.keyField == textField) {
        [self.keyField resignFirstResponder];
        [self.valueField becomeFirstResponder];
        return YES;
    } else if (self.valueField == textField) {
        if (self.valueField.text.length > 0) {
            [self.valueField resignFirstResponder];
            return YES;
        }
    }
    return NO;
}

@end
