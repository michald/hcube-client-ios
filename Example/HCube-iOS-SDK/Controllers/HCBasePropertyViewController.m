//
//  HCBasePropertyViewController.m
//  HCube-iOS-SDK
//
//  Created by Dmitriy Karachentsov on 3/10/16.
//  Copyright © 2016 Dmitriy Karachentsov. All rights reserved.
//

#import "HCBasePropertyViewController.h"

@interface HCBasePropertyViewController () <UITableViewDelegate, UITableViewDataSource, HCPropertyCellDelegate>

@end

@implementation HCBasePropertyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

#pragma mark - Public

- (void)addProperty {
    HCEventProperty *property = [HCEventProperty new];
    [self.properties addObject:property];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.properties.count - 1 inSection:0];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationTop];
}

- (NSDictionary *)propertiesDictionary {
    NSMutableDictionary *propertiesDictionary = [NSMutableDictionary new];
    for (HCEventProperty *property in self.properties) {
        [propertiesDictionary addEntriesFromDictionary:[property serializedObject]];
    }
    return [propertiesDictionary copy];
}

#pragma mark - Private

- (void)fillPropertyCell:(HCPropertyCell *)propertyCell atIndexPath:(NSIndexPath *)indexPath {
    HCEventProperty *property = self.properties[indexPath.row];
    propertyCell.keyField.text = property.key;
    propertyCell.valueField.text = property.value;
    propertyCell.delegate = self;
}

- (BOOL)checkCellsAndPresentAlertIfNeeded {
    UIAlertController *alertController = nil;
    
    if (![self allCellsAreFilled]) {
        alertController = [UIAlertController
                           alertControllerWithTitle:@"Cells are not filled!"
                           message:@"Please, fill all cells."
                           preferredStyle:UIAlertControllerStyleAlert];
    }
    
    if (alertController) {
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        [alertController addAction:action];
        [self presentViewController:alertController animated:YES completion:nil];
        return NO;
    }
    return YES;
}

- (BOOL)allCellsAreFilled {
    BOOL allCellsAreFilled = YES;
    for (HCEventProperty *property in self.properties) {
        if (property.key.length == 0
            || property.value.length == 0) {
            allCellsAreFilled = NO;
            break;
        }
    }
    return allCellsAreFilled;
}

#pragma mark - Custom Accessors

#pragma mark Getters

- (NSMutableArray *)properties {
    if (!_properties) {
        _properties = [NSMutableArray new];
    }
    return _properties;
}

- (HCube *)hcube {
    __weak HCube *hcube = [HCube sharedInstance];
    return hcube;
}

#pragma mark - <UITableViewDelegate>

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.properties removeObjectAtIndex:indexPath.row];
        [self.tableView deleteRowsAtIndexPaths:@[indexPath]
                              withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

#pragma mark - <UITableViewDataSource>

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.properties count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    HCPropertyCell *propertyCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([HCPropertyCell class]) forIndexPath:indexPath];
    [self fillPropertyCell:propertyCell atIndexPath:indexPath];
    return propertyCell;
}

#pragma mark - <HCPropertyCellDelegate>

- (void)trackPropertyCell:(HCPropertyCell *)cell keyDidChanged:(NSString *)key {
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    HCEventProperty *property = self.properties[indexPath.row];
    property.key = key;
}
- (void)trackPropertyCell:(HCPropertyCell *)cell valueDidChanged:(NSString *)value {
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    HCEventProperty *property = self.properties[indexPath.row];
    property.value = value;
}

@end
