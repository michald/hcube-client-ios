//
//  HCCommonViewController.m
//  HCube-iOS-SDK
//
//  Created by Dmitriy Karachentsov on 2/10/16.
//  Copyright © 2016 Dmitriy Karachentsov. All rights reserved.
//

#import "HCCommonViewController.h"
#import "HCPropertyCell.h"
#import "HCEventProperty.h"

#import <HCube_iOS_SDK/HCube.h>

@interface HCCommonViewController ()

@end

@implementation HCCommonViewController

@dynamic hcube;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadCommonProperties];
}

#pragma mark - @IBActions

- (IBAction)addAction:(id)sender {
    [self addProperty];
}

- (IBAction)saveAction:(id)sender {
    [self saveProperties];
}

#pragma mark - Private

- (void)loadCommonProperties {
    
    NSMutableArray *indexPaths = [NSMutableArray new];
    
    NSDictionary *dictionary = [self.hcube getCommonProperties];
    for (NSString *key in dictionary.allKeys) {
        NSString *value = dictionary[key];
        
        HCEventProperty *property = [HCEventProperty new];
        property.key = key;
        property.value = value;
        [self.properties addObject:property];
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.properties.count - 1 inSection:0];
        [indexPaths addObject:indexPath];
    }
    [self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
}

- (void)saveProperties {
    if ([self checkCellsAndPresentAlertIfNeeded]) {
        [self.hcube clearAllCommonProperties];
        
        [self.hcube addCommonProperties:self.propertiesDictionary];
    }
}

@end
