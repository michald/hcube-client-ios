//
//  HCTrackViewController.m
//  HCube-iOS-SDK
//
//  Created by Dmitriy Karachentsov on 2/10/16.
//  Copyright © 2016 Dmitriy Karachentsov. All rights reserved.
//

#import "HCTrackViewController.h"

NSString *const HCTrackEventKeyFieldKey = @"EVENT_KEY";
NSString *const HCTrackEventValueFieldKey = @"EVENT_VALUE";

@interface HCTrackViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *eventNameField;

@end

@implementation HCTrackViewController

@dynamic hcube;

- (void)viewDidLoad {
    [super viewDidLoad];
}

#pragma mark - @IBActions

- (IBAction)addPropertyAction:(UIBarButtonItem *)sender {
    [self addProperty];
}

- (IBAction)sendEventAction:(UIBarButtonItem *)sender {
    [self sendEvent];
}

#pragma mark - Private

- (BOOL)checkCellsAndPresentAlertIfNeeded {
    if (![super checkCellsAndPresentAlertIfNeeded]) {
        return NO;
    }
    
    UIAlertController *alertController = nil;
    
    if (self.eventNameField.text.length == 0) {
        alertController = [UIAlertController
                           alertControllerWithTitle:@"Event name is empty!"
                           message:@"Please, fill event name."
                           preferredStyle:UIAlertControllerStyleAlert];
    }
    
    if (alertController) {
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        [alertController addAction:action];
        [self presentViewController:alertController animated:YES completion:nil];
        return NO;
    }
    return YES;
}

- (void)sendEvent {
    if ([self checkCellsAndPresentAlertIfNeeded]) {
        NSDictionary *propertiesDictionary = [self propertiesDictionary];
        
        [self.hcube trackEvent:self.eventNameField.text properties:propertiesDictionary];
    }
}

#pragma mark - <UITextFieldDelegate>

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (self.eventNameField == textField) {
        return YES;
    }
    return NO;
}

@end
