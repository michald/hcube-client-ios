//
//  HCBasePropertyViewController.h
//  HCube-iOS-SDK
//
//  Created by Dmitriy Karachentsov on 3/10/16.
//  Copyright © 2016 Dmitriy Karachentsov. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HCPropertyCell.h"
#import "HCEventProperty.h"
#import <HCube_iOS_SDK/HCube.h>

@interface HCBasePropertyViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSMutableArray <HCEventProperty *>* properties;
@property (nonatomic, weak) HCube *hcube;

- (void)addProperty;

- (NSDictionary *)propertiesDictionary;

- (BOOL)checkCellsAndPresentAlertIfNeeded;

@end
