//
//  HCCommonViewController.h
//  HCube-iOS-SDK
//
//  Created by Dmitriy Karachentsov on 2/10/16.
//  Copyright © 2016 Dmitriy Karachentsov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCBasePropertyViewController.h"

@interface HCCommonViewController : HCBasePropertyViewController

@end
