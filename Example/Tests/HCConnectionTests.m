//
//  HCConnectionTests.m
//  HCube-iOS-SDK
//
//  Created by Dmitriy Karachentsov on 14/10/16.
//  Copyright © 2016 Dmitriy Karachentsov. All rights reserved.
//

#import "Specta/Specta.h"
#import "Expecta/Expecta.h"
#import <OCMock/OCMock.h>
#import <XCTest/XCTest.h>
#import <HCube_iOS_SDK/HCube.h>
#import <HCube_iOS_SDK/HCubePrivate.h>
#import <HCube_iOS_SDK/HCConnection.h>
#import <HCube_iOS_SDK/HCEvent.h>
#import <HCube_iOS_SDK/HCConnectionPrivate.h>

SpecBegin(HCConnectionTests)

describe(@"connection", ^{
    
    NSString *const URL = @"http://google.com/";
    
    it(@"should build URL path by qualify endpoint", ^{
        HCube *cube = [HCube provideWithToken:@"<TOKEN>"];
        NSString *customPath = @"path0/path1/path2";
        
        NSString *URLString = [NSString stringWithFormat:URL];
        URLString = [URLString stringByAppendingFormat:@"/%@",customPath];
        NSURL *url = [NSURL URLWithString:URLString];
        HCConnection *connection = [[HCConnection alloc] initWithHCube:cube baseURL:url];
        NSString *path = [connection URLPathByEndpoint:HCConnectionQualifyEndpoint];
        expect(path).to.equal([NSString stringWithFormat:@"/%@/%@/%@",customPath, HCConnectionQualifyEndpoint, cube.uniqueId ]);
    });
    
    it(@"should build URL path by event endpoint", ^{
        HCube *cube = [HCube provideWithToken:@"<TOKEN>"];
        NSString *customPath = @"path0/path1/path2";
        
        NSString *URLString = [NSString stringWithFormat:URL];
        URLString = [URLString stringByAppendingFormat:@"/%@",customPath];
        NSURL *url = [NSURL URLWithString:URLString];
        HCConnection *connection = [[HCConnection alloc] initWithHCube:cube baseURL:url];
        NSString *path = [connection URLPathByEndpoint:HCConnectionEventsEndpoint];
        expect(path).to.equal([NSString stringWithFormat:@"/%@/%@",customPath, HCConnectionEventsEndpoint ]);
    });
    
    it(@"should build request with object and qualify endpoint", ^{
        HCube *cube = [HCube provideWithToken:@"<TOKEN>"];
        NSString *uniqueId = @"12345-12345-12345-12345";
        [cube identify:uniqueId];
        
        waitUntil(^(DoneCallback done) {
            dispatch_async(cube.serialQueue, ^{
                done();
            });
        });
        
        NSURL *url = [NSURL URLWithString:URL];
        HCConnection *connection = [[HCConnection alloc] initWithHCube:cube baseURL:url];
        
        HCEvent *event = [[HCEvent alloc] initWithName:@"EVENT" properties:@{@"String":@"String"}];
        NSURLRequest *request = [connection buildRequestWithObject:event endpoint:HCConnectionQualifyEndpoint];
        
        NSURLComponents *components = [[NSURLComponents alloc] initWithURL:request.URL resolvingAgainstBaseURL:YES];
        
        expect(components.path).to.equal([NSString stringWithFormat:@"/%@/%@/%@",HCConnectionQualifyEndpoint,uniqueId, @"eyJTdHJpbmciOiJTdHJpbmciLCJzdGF0ZV9uYW1lIjoiRVZFTlQifQ=="]);
    });
    
    it(@"should build request with object and events endpoint", ^{
        HCube *cube = [HCube provideWithToken:@"<TOKEN>"];
        NSURL *url = [NSURL URLWithString:URL];
        HCConnection *connection = [[HCConnection alloc] initWithHCube:cube baseURL:url];
        
        HCEvent *event = [[HCEvent alloc] initWithName:@"EVENT" properties:@{@"String":@"String"}];
        NSURLRequest *request = [connection buildRequestWithObject:event endpoint:HCConnectionEventsEndpoint];
        
        NSURLComponents *components = [[NSURLComponents alloc] initWithURL:request.URL resolvingAgainstBaseURL:YES];
        expect(components.path).to.equal(@"/input/event/send/eyJTdHJpbmciOiJTdHJpbmciLCJzdGF0ZV9uYW1lIjoiRVZFTlQifQ==");
    });
    
    it(@"should check success response and return true", ^{
        HCube *cube = [HCube provideWithToken:@"<TOKEN>"];
        NSURL *url = [NSURL URLWithString:URL];
        HCConnection *connection = [[HCConnection alloc] initWithHCube:cube baseURL:url];
        
        NSHTTPURLResponse *response = [NSHTTPURLResponse new];
        id mock = OCMPartialMock(response);
        
        OCMStub([mock statusCode]).andReturn(200);
        
        expect([connection checkResponse:response withError:nil]).to.beTruthy();
    });
    
    it(@"should check fail response and return false", ^{
        HCube *cube = [HCube provideWithToken:@"<TOKEN>"];
        NSURL *url = [NSURL URLWithString:URL];
        HCConnection *connection = [[HCConnection alloc] initWithHCube:cube baseURL:url];
        
        NSHTTPURLResponse *response = [NSHTTPURLResponse new];
        id mock = OCMPartialMock(response);
        
        OCMStub([mock statusCode]).andReturn(404);
        
        NSError *error = [NSError errorWithDomain:@"com.hcube.tests" code:0 userInfo:nil];
        expect([connection checkResponse:response withError:error]).to.beFalsy();
    });
    
    it(@"should send event on the dispatch queue", ^{
        HCube *cube = [HCube provideWithToken:@"<TOKEN>"];
        NSURL *url = [NSURL URLWithString:URL];
        HCConnection *connection = [[HCConnection alloc] initWithHCube:cube baseURL:url];
        id mock = OCMPartialMock(connection.session);
        
        __block BOOL dataTaskWithRequestDidCalled = NO;
        OCMStub([mock dataTaskWithRequest:[OCMArg any] completionHandler:[OCMArg invokeBlock]]).andDo(^(NSInvocation *invocation){
            dataTaskWithRequestDidCalled = YES;
        });
        
        connection.networkReachable = YES;
        
        HCEvent *event = [[HCEvent alloc] initWithName:@"EVENT" properties:@{@"String":@"String"}];
        NSMutableArray *array = [@[event] mutableCopy];
        [connection dispatchQueue:array withEndpoint:HCConnectionEventsEndpoint];
        
        expect(dataTaskWithRequestDidCalled).to.beTruthy();
    });
    
    it(@"should send empty array on the dispatch queue", ^{
        HCube *cube = [HCube provideWithToken:@"<TOKEN>"];
        NSURL *url = [NSURL URLWithString:URL];
        HCConnection *connection = [[HCConnection alloc] initWithHCube:cube baseURL:url];
        id mock = OCMPartialMock(connection.session);
        
        __block BOOL dataTaskWithRequestDidCalled = NO;
        OCMStub([mock dataTaskWithRequest:[OCMArg any] completionHandler:[OCMArg invokeBlock]]).andDo(^(NSInvocation *invocation){
            dataTaskWithRequestDidCalled = YES;
        });
        
        connection.networkReachable = YES;
        
        NSMutableArray *array = [@[] mutableCopy];
        [connection dispatchQueue:array withEndpoint:HCConnectionEventsEndpoint];
        
        expect(dataTaskWithRequestDidCalled).to.beFalsy();
    });
    
});

SpecEnd
