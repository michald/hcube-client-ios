//
//  HCubeTests.m
//  HCube-iOS-SDK
//
//  Created by Dmitriy Karachentsov on 5/10/16.
//  Copyright © 2016 Dmitriy Karachentsov. All rights reserved.
//

#import "Specta/Specta.h"
#import <Expecta/Expecta.h>
#import <OCMock/OCMock.h>
#import <HCube_iOS_SDK/HCube.h>
#import <HCube_iOS_SDK/HCube+Persistence.h>
#import <HCube_iOS_SDK/HCEvent.h>
#import <HCube_iOS_SDK/HCubePrivate.h>

SpecBegin(HCubeTests)

describe(@"hcube", ^{
    
    NSDictionary *const TestsProperties1 = @{
        @"Product": @"iPhone 6s",
        @"Price": @700.00,
        @"Description": @"Какой-то текст"
    };
    
    beforeEach(^{
        NSString *token = @"<MY_SECRET_TOKEN>";
        
        HCube *cube = [HCube provideWithToken:token];
        
        [cube clearAllCommonProperties];
    });
    
    it(@"should change dispatch interval", ^{
        
        NSInteger const Interval = 3;
        NSString *token = @"<MY_SECRET_TOKEN>";
        HCube *cube = [HCube provideWithToken:token];
        cube.dispatchInterval = Interval;
        
        HCube *mockCube = OCMPartialMock(cube);

        dispatch_semaphore_t signal = dispatch_semaphore_create(0);
        OCMStub([mockCube dispatch]).andDo(^(NSInvocation *invocation){
            dispatch_semaphore_signal(signal);
        });
        
        HCEvent *event = [[HCEvent alloc] initWithName:@"Event" properties:nil];
        [cube.eventsQueue addObject:event];
        
        NSDate *date1 = [NSDate date];
        __block NSDate *date2 = nil;
        
        waitUntil(^(DoneCallback done) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                dispatch_semaphore_wait(signal, DISPATCH_TIME_FOREVER);
                dispatch_async(dispatch_get_main_queue(), ^{
                    date2 = [NSDate date];
                    done();
                });
            });
        });
        
        NSTimeInterval workTimeInterval = [date2 timeIntervalSinceDate:date1];
        expect(floor(workTimeInterval)).to.equal(Interval);
    });
    
    it(@"should change uniqueId", ^{
        NSString *const Identifier = @"1234567890";
        NSString *token = @"<MY_SECRET_TOKEN>";
        HCube *cube = [HCube provideWithToken:token];
        [cube identify:Identifier];
        
        waitUntil(^(DoneCallback done) {
            dispatch_async(cube.serialQueue, ^{
                done();
            });
        });
        
        expect(cube.uniqueId).to.equal(Identifier);
    });
    
    it(@"should add event to queue", ^{
        NSString *token = @"<MY_SECRET_TOKEN>";
        HCube *cube = [HCube provideWithToken:token];
        cube.eventsQueue = [NSMutableArray array];
        [cube archiveEvents];
        
        HCube *mockCube = OCMPartialMock(cube);
        OCMStub([mockCube dispatch]).andDo(nil);
        
        NSString *EventName = @"EVENT_NAME";
        [cube trackEvent:EventName properties:TestsProperties1];
        
        waitUntil(^(DoneCallback done) {
            dispatch_async(cube.serialQueue, ^{
                done();
            });
        });
        
        HCEvent *event = [cube.eventsQueue firstObject];
        expect(event.eventName).to.equal(EventName);
        expect(event.properties).to.beSupersetOf(TestsProperties1);
    });
    
    it(@"should add event without properties to queue", ^{
        NSString *token = @"<MY_SECRET_TOKEN>";
        HCube *cube = [HCube provideWithToken:token];
        cube.eventsQueue = [NSMutableArray array];
        [cube archiveEvents];
        
        HCube *mockCube = OCMPartialMock(cube);
        OCMStub([mockCube dispatch]).andDo(nil);
        
        NSString *EventName = @"EVENT_NAME";
        [cube trackEvent:EventName];
        
        waitUntil(^(DoneCallback done) {
            dispatch_async(cube.serialQueue, ^{
                done();
            });
        });
        
        HCEvent *event = [cube.eventsQueue firstObject];
        expect(event.eventName).to.equal(EventName);
    });
    
    it(@"should add common properties", ^{
        NSString *token = @"<MY_SECRET_TOKEN>";
        HCube *cube = [HCube provideWithToken:token];
        cube.eventsQueue = [NSMutableArray array];
        [cube archiveEvents];
        
        [cube addCommonProperties:TestsProperties1];
        
        waitUntil(^(DoneCallback done) {
            dispatch_async(cube.serialQueue, ^{
                done();
            });
        });
        
        NSDictionary *commonProperties = [cube getCommonProperties];
        expect(commonProperties).to.equal(TestsProperties1);
    });
    
    it(@"should add common properties once", ^{
        NSString *token = @"<MY_SECRET_TOKEN>";
        HCube *cube = [HCube provideWithToken:token];
        NSDictionary *dictionary1 = @{
            @"String":@"String_1",
            @"Number":@1234
        };
        
        [cube addCommonPropertiesOnce:dictionary1];
        
        waitUntil(^(DoneCallback done) {
            dispatch_async(cube.serialQueue, ^{
                done();
            });
        });
        
        NSDictionary *dictionary2 = @{
            @"String":@"String_2",
            @"Number":@4321
        };
        
        [cube addCommonPropertiesOnce:dictionary2];
        
        waitUntil(^(DoneCallback done) {
            dispatch_async(cube.serialQueue, ^{
                done();
            });
        });
        
        NSDictionary *commonProperties = [cube getCommonProperties];
        expect(commonProperties).to.equal(dictionary1);
    });
    
    it(@"should remove common properties", ^{
        NSString *token = @"<MY_SECRET_TOKEN>";
        HCube *cube = [HCube provideWithToken:token];
        
        [cube addCommonProperties:TestsProperties1];
        
        waitUntil(^(DoneCallback done) {
            dispatch_async(cube.serialQueue, ^{
                done();
            });
        });
        
        NSDictionary *commonProperties = [cube getCommonProperties];
        expect(commonProperties).haveCount(3);
        
        [cube removeCommonPropertyByName:@"Product"];
        
        waitUntil(^(DoneCallback done) {
            dispatch_async(cube.serialQueue, ^{
                done();
            });
        });

        commonProperties = [cube getCommonProperties];
        expect([commonProperties.allKeys containsObject:@"Product"]).beFalsy();
        expect([commonProperties.allKeys containsObject:@"Price"]).beTruthy();
        expect([commonProperties.allKeys containsObject:@"Description"]).beTruthy();
    });
    
    it(@"should clear all common properties", ^{
        NSString *token = @"<MY_SECRET_TOKEN>";
        HCube *cube = [HCube provideWithToken:token];
        
        [cube addCommonProperties:TestsProperties1];
        
        waitUntil(^(DoneCallback done) {
            dispatch_async(cube.serialQueue, ^{
                done();
            });
        });
        
        NSDictionary *commonProperties = [cube getCommonProperties];
        expect(commonProperties).haveCount(3);
        
        [cube clearAllCommonProperties];
        
        waitUntil(^(DoneCallback done) {
            dispatch_async(cube.serialQueue, ^{
                done();
            });
        });
        
        commonProperties = [cube getCommonProperties];
        expect(commonProperties).haveCount(0);
    });
    
    it(@"should get common properties", ^{
        NSString *token = @"<MY_SECRET_TOKEN>";
        HCube *cube = [HCube provideWithToken:token];
        
        NSDictionary *commonProperties = [cube getCommonProperties];
        expect(commonProperties).to.equal(@{ });
        
        [cube addCommonProperties:TestsProperties1];
        
        waitUntil(^(DoneCallback done) {
            dispatch_async(cube.serialQueue, ^{
                done();
            });
        });
        
        commonProperties = [cube getCommonProperties];
        expect(commonProperties).to.equal(TestsProperties1);
    });
    
});

SpecEnd

