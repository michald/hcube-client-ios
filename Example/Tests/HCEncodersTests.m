//
//  HCEncodersTests.m
//  HCube-iOS-SDK
//
//  Created by Dmitriy Karachentsov on 13/10/16.
//  Copyright © 2016 Dmitriy Karachentsov. All rights reserved.
//

#import "Specta/Specta.h"
#import "Expecta/Expecta.h"
#import <HCube_iOS_SDK/HCEncoders.h>

SpecBegin(HCEncodersTests)

describe(@"encoders", ^{
    
    it(@"should return correct JSONData", ^{
        NSDictionary *dictionary = @{ @"firstName":@"User1" };
        NSData *jsonData = [dictionary JSONData];
        NSString *string = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        expect(string).to.equal(@"{\"firstName\":\"User1\"}");
    });
    
    it(@"should return uncorrect JSONData", ^{
        NSString *string = @"STRING";
        NSData *jsonData = [string JSONData];
        expect(jsonData).to.beNil();
    });
    
    it(@"should return correct base64", ^{
        //Base64 for that data:
        //{"firstName":"User1"}
        NSString *base64 = @"eyJmaXJzdE5hbWUiOiJVc2VyMSJ9";
        NSDictionary *dictionary = @{ @"firstName":@"User1" };
        NSString *base64ForDictionary = [[dictionary JSONData] base64];
        expect(base64).to.equal(base64ForDictionary);
    });
    
    it(@"should return empty string if base64 is empty", ^{
        //Base64 for that data:
        //{"firstName":"User1"}
        NSData *data = [NSData data];
        NSString *base64 = [data base64];
        expect(base64).to.beNil();
    });

    
});

SpecEnd
