//
//  HCQualifyTests.m
//  HCube-iOS-SDK
//
//  Created by Dmitriy Karachentsov on 11/10/16.
//  Copyright © 2016 Dmitriy Karachentsov. All rights reserved.
//

#import "Specta/Specta.h"
#import "Expecta/Expecta.h"
#import <OCMock/OCMock.h>
#import <HCube_iOS_SDK/HCQualify.h>
#import <HCube_iOS_SDK/HCQualifyPrivate.h>
#import <HCube_iOS_SDK/HCube.h>
#import <HCube_iOS_SDK/HCube+Persistence.h>
#import <XCTest/XCTest.h>

@interface StrangeObject : NSObject <NSCopying>
@end

@implementation StrangeObject

- (NSInteger)length {
    return 10;
}

- (id)copyWithZone:(nullable NSZone *)zone {
    id object = [[self class] allocWithZone:zone];
    return object;
}

@end

SpecBegin(HCQualifyTests)

describe(@"qualify", ^{
    
    NSDictionary *const TestsProperties1 = @{
        @"Product": @"iPhone 6s",
        @"Price": @700.00,
        @"Описание": @"Какой-то текст"
    };
    
    NSDictionary *const TestsProperties2 = @{
        @"Average": @"30",
        @"Number": @11.564932,
        @"Dictionary": @{
                @"name" : @"Alex",
                @"relationships": @"married"
        }
    };
    
    beforeEach(^{
        HCube *cube = [[HCube alloc] initWithToken:@"<SECRET_TOKEN>"];
        cube.qualifyQueue = [NSMutableArray new];
        [cube archiveQualifies];
        
        waitUntil(^(DoneCallback done) {
            dispatch_async(cube.serialQueue, ^{
                done();
            });
        });
    });
    
    it(@"should set properties", ^{
        HCQualify *qualify = [[HCQualify alloc] initWithHCube:nil];
        [qualify setProperties:TestsProperties1];
        expect(qualify.qualifies).to.beSupersetOf(TestsProperties1);
    });
    
    it(@"should set property with object", ^{
        HCQualify *qualify = [[HCQualify alloc] initWithHCube:nil];
        [qualify setProperty:@"Product" withObject:@"iPhone 6s"];
        expect(qualify.qualifies).to.beSupersetOf(@{@"Product": @"iPhone 6s"});
    });
    
    it(@"should clear qualify", ^{
        HCQualify *qualify = [[HCQualify alloc] initWithHCube:nil];
        [qualify setProperties:TestsProperties1];
        [qualify setProperty:@"Average" withObject:@"30"];
        [qualify setProperty:@"Number" withObject:@11.564932];
        
        [qualify clearQualify];
        
        expect(qualify.qualifies).to.beEmpty();
    });
    
    it(@"should add all of properties", ^{
        HCQualify *qualify = [[HCQualify alloc] initWithHCube:nil];
        [qualify setProperties:TestsProperties1];
        [qualify setProperty:@"Average" withObject:@"30"];
        [qualify setProperty:@"Number" withObject:@11.564932];
        
        NSInteger keysCount = TestsProperties1.count + 2;
        expect(qualify.qualifies).to.haveCountOf(keysCount);
    });
    
    it(@"shouldn't add non-dictionaries to properties", ^{
        HCQualify *qualify = [[HCQualify alloc] initWithHCube:nil];
        NSArray *object = [NSArray new];
        [qualify setProperties:(NSDictionary *)object];
        expect(qualify.qualifies).to.beEmpty();
    });
    
    it(@"shouldn't add object with empty key", ^{
        HCQualify *qualify = [[HCQualify alloc] initWithHCube:nil];
        [qualify setProperty:nil withObject:@"30"];
        expect(qualify.qualifies).to.beEmpty();
    });
    
    it(@"shouldn't add key with empty object", ^{
        HCQualify *qualify = [[HCQualify alloc] initWithHCube:nil];
        NSString *const KeyName = @"Key";
        [qualify setProperty:KeyName withObject:nil];
        
        expect(qualify.qualifies[KeyName]).to.beNil();
    });
    
    it(@"shouldn't add property with non-string key", ^{
        HCQualify *qualify = [[HCQualify alloc] initWithHCube:nil];
        StrangeObject *object = [StrangeObject new];
        [qualify setProperty:(NSString *)object withObject:@"String"];
        expect(qualify.qualifies).to.beEmpty();
    });
    
    it(@"shouldn't add dictionaries with non-string keys", ^{
        HCQualify *qualify = [[HCQualify alloc] initWithHCube:nil];
        StrangeObject *object = [StrangeObject new];
        [qualify setProperties:@{object:@"String"}];
        expect(qualify.qualifies).to.beEmpty();
    });
    
    it(@"shouldn't add dictionary with strange object value", ^{
        HCQualify *qualify = [[HCQualify alloc] initWithHCube:nil];
        StrangeObject *object = [StrangeObject new];
        [qualify setProperties:@{@"String":object}];
        expect(qualify.qualifies).to.beEmpty();
    });
    
    it(@"shouldn't add some properties", ^{
        HCQualify *qualify = [[HCQualify alloc] initWithHCube:nil];
        [qualify setProperties:TestsProperties2];
        expect(qualify.qualifies).to.haveCount(2);
    });
    
    it(@"should add properties with permitted values", ^{
        HCQualify *qualify = [[HCQualify alloc] initWithHCube:nil];
        
        NSNumber *number = @34521;
        NSString *string = @"Some string";
        NSDate *date = [NSDate date];
        NSURL *url = [NSURL URLWithString:@"http://www.google.com"];
        StrangeObject *strangeObject = [StrangeObject new];
        
        //Should add
        [qualify setProperty:@"NSNumber" withObject:number];
        [qualify setProperty:@"NSString" withObject:string];
        [qualify setProperty:@"NSDate" withObject:date];
        [qualify setProperty:@"NSURL" withObject:url];
        
        [qualify setProperty:@"StrangeObject" withObject:strangeObject];
        
        expect(qualify.qualifies).to.haveCount(4);
        expect(qualify.qualifies[@"NSNumber"]).to.equal(number);
        expect(qualify.qualifies[@"NSString"]).to.equal(string);
        expect(qualify.qualifies[@"NSDate"]).to.equal(date);
        expect(qualify.qualifies[@"NSURL"]).to.equal(url);
    });
    
    it(@"should copy object", ^{
        HCube *cube = [[HCube alloc] initWithToken:@"<SECRET_TOKEN>"];
        HCQualify *qualify = [[HCQualify alloc] initWithHCube:cube];
        [qualify setProperties:TestsProperties1];
        HCQualify *newQualify = [qualify copy];
        expect(qualify.qualifies).equal(newQualify.qualifies);
        expect(qualify.cube).equal(newQualify.cube);
    });
    
    it(@"should serialize object", ^{
        HCQualify *qualify = [[HCQualify alloc] initWithHCube:nil];
        
        NSNumber *number = @34521;
        NSString *string = @"Some string";
        NSDate *date = [NSDate date];
        NSURL *url = [NSURL URLWithString:@"http://www.google.com"];
        
        [qualify setProperty:@"NSNumber" withObject:number];
        [qualify setProperty:@"NSString" withObject:string];
        [qualify setProperty:@"NSDate" withObject:date];
        [qualify setProperty:@"NSURL" withObject:url];
        
        NSDictionary *serializedObject = [qualify serializedObject];
        
        expect(serializedObject[@"uniqueDetails"]).to.beKindOf([NSArray class]);
        expect(serializedObject[@"uniqueDetails"]).to.haveCount(4);
        
        NSDictionary *firstElement = serializedObject[@"uniqueDetails"][0];
        
        expect(firstElement).to.beKindOf([NSDictionary class]);
        expect(firstElement.allKeys).contain(@"ukey");
        expect(firstElement.allKeys).contain(@"uvalue");
    });
    
    it(@"should be empty if object is empty", ^{
        HCQualify *qualify = [[HCQualify alloc] initWithHCube:nil];
        expect([qualify isEmpty]).to.beTruthy();
    });
    
    it(@"should be not empty if object isn't empty", ^{
        HCQualify *qualify = [[HCQualify alloc] initWithHCube:nil];
        
        NSNumber *number = @34521;
        NSString *string = @"Some string";
        NSDate *date = [NSDate date];
        NSURL *url = [NSURL URLWithString:@"http://www.google.com"];
        
        [qualify setProperty:@"NSNumber" withObject:number];
        [qualify setProperty:@"NSString" withObject:string];
        [qualify setProperty:@"NSDate" withObject:date];
        [qualify setProperty:@"NSURL" withObject:url];
        
        expect([qualify isEmpty]).to.beFalsy();
    });
    
    it(@"should add qualifies to hcube queue", ^{
        HCube *cube = [[HCube alloc] initWithToken:@"<SECRET_TOKEN>"];
        HCQualify *qualify = [[HCQualify alloc] initWithHCube:cube];
        [qualify addQualifyInQueue];
        
        NSNumber *number = @34521;
        NSString *string = @"Some string";
        NSDate *date = [NSDate date];
        NSURL *url = [NSURL URLWithString:@"http://www.google.com"];
        
        [qualify setProperty:@"NSNumber" withObject:number];
        [qualify setProperty:@"NSString" withObject:string];
        [qualify setProperty:@"NSDate" withObject:date];
        [qualify setProperty:@"NSURL" withObject:url];
        
        [qualify addQualifyInQueue];
        [qualify addQualifyInQueue];
        
        expect(cube.qualifyQueue.count).equal(7);
    });
    
    it(@"should add qualify after adding one property", ^{
        HCube *cube = [[HCube alloc] initWithToken:@"<SECRET_TOKEN>"];
        HCQualify *qualify = [[HCQualify alloc] initWithHCube:cube];
        id mockQualify = OCMPartialMock(qualify);
        
        __block BOOL addQualifyInQueueDidCalled = NO;
        OCMStub([mockQualify addQualifyInQueue]).andDo(^(NSInvocation *invocation) {
            addQualifyInQueueDidCalled = YES;
        });
        
        [qualify setProperty:@"String" withObject:@"String"];
        
        expect(addQualifyInQueueDidCalled).to.beTruthy();
    });
    it(@"should add qualify after adding properties", ^{
        HCube *cube = [[HCube alloc] initWithToken:@"<SECRET_TOKEN>"];
        HCQualify *qualify = [[HCQualify alloc] initWithHCube:cube];
        id mockQualify = OCMPartialMock(qualify);
        
        __block BOOL addQualifyInQueueDidCalled = NO;
        OCMStub([mockQualify addQualifyInQueue]).andDo(^(NSInvocation *invocation) {
            addQualifyInQueueDidCalled = YES;
        });
        
        [qualify setProperties:TestsProperties1];
        expect(addQualifyInQueueDidCalled).to.beTruthy();
    });

    it(@"should archive object", ^{
        HCQualify *qualify = [[HCQualify alloc] initWithHCube:nil];
        [qualify setProperty:@"string_1" withObject:@"string_1"];
        [qualify setProperty:@"string_2" withObject:@"string_2"];
        [qualify setProperty:@"string_3" withObject:@"string_3"];
        
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:qualify];
        expect(data.length > 0).beTruthy();
    });
    
});

SpecEnd

//        HCQualify *qualify = [[HCQualify alloc] initWithHCube:nil];
//        id mockQualify = OCMPartialMock(qualify);
//        OCMStub([mockQualify addQualifyInQueue]).andDo(^(NSInvocation *invocation) {
//
//        });failure(@"");
