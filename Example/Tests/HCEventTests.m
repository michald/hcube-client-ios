//
//  HCube-iOS-SDKTests.m
//  HCube-iOS-SDKTests
//
//  Created by Dmitriy Karachentsov on 09/29/2016.
//  Copyright (c) 2016 Dmitriy Karachentsov. All rights reserved.
//

#import "Specta/Specta.h"
#import "Expecta/Expecta.h"
#import <HCube_iOS_SDK/HCEvent.h>

SpecBegin(HCEventTests)

describe(@"event", ^{
    
    NSString *eventName = @"Test Event";
    NSDictionary *eventProperties = @{
        @"Product": @"iPhone 6s",
        @"Price": @700.00
    };
    
    HCEvent *event = [[HCEvent alloc] initWithName:eventName properties:eventProperties];
    
    it(@"should has equal properties", ^{
        expect(event.eventName).to.equal(eventName);
        expect(event.properties).to.equal(eventProperties);
    });
    
    it(@"should return serialized objects with properties", ^{
        NSDictionary *serializedObject = [event serializedObject];
        expect(serializedObject).to.beSupersetOf(eventProperties);
    });
    
    it(@"should has event name in serialized object", ^{
        NSDictionary *serializedObject = [event serializedObject];
        NSDictionary *nameDict = @{ @"state_name" : eventName };
        expect(serializedObject).to.beSupersetOf(nameDict);
    });
    
});

SpecEnd

