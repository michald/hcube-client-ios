# Require Xcodeproj
require "fileutils"
begin
  gem 'xcodeproj', '= 1.4.1'
  require "xcodeproj"
rescue LoadError
  puts "Installing Xcodeproj..."
  `gem install xcodeproj --version '1.4.1'`
  puts "Xcodeproj installed"
  require "xcodeproj"
end

SDK_DIRECTORY_NAME = "HCube-iOS-SDK"

puts "Welcome to HCube SDK Installer"
puts "\n"
puts "Enter your project directory: "
puts "Example: \"~/Documents/SuperProject\""

print "> ";
begin
  project_path = Dir[File.expand_path(gets.chomp.strip)].first
  xcodeproj_path = File.join(
    project_path,
    "*.xcodeproj"
    )
  xcodeproj_file = Dir.glob(xcodeproj_path).first
  project = Xcodeproj::Project.open(xcodeproj_file)
rescue Exception => e
  puts "Sorry, I didn't find any xcodeproj"
  puts "[FAIL]"
  exit()
end

puts "\n"
puts "OK, I found #{File.basename(xcodeproj_file)}"
puts "Select target:"
project.targets.each_with_index do |target, index|
  puts "#{index+1}) #{target.name}"
end
print "> ";
target_index = gets.chomp.to_i
target = project.targets[target_index-1]

p project_path

FileUtils.cp_r File.absolute_path(SDK_DIRECTORY_NAME), project_path
puts "SDK сopied into the \"#{target.name}\" resources"

group = project.new_group(
  SDK_DIRECTORY_NAME,
  File.join(project_path, SDK_DIRECTORY_NAME)
  )

Dir["#{SDK_DIRECTORY_NAME}/**/*"].each do |path|
  if File.file?(path)
    file_reference = group.new_reference(File.join(project_path, path))
    target.source_build_phase.add_file_reference(file_reference)
  end
end
project.save
puts "SDK files added to the project"
puts "[DONE]"
