//
//  HCConnection.h
//

#import <Foundation/Foundation.h>

@class HCEvent;
@class HCQualify;
@class HCube;

@interface HCConnection : NSObject

/**
 Base URL
 */
@property (nonatomic, strong, readonly) NSURL *baseURL;

/**
 Reachability status property.
 */
@property (nonatomic, readonly, assign, getter=isNetworkReachable) BOOL networkReachable;


/**
 Initializes and returns a newly created HCConnection instance

 @param hcube   HCube instance
 @param baseURL Base URL

 @return HCConnection instance
 */
- (instancetype)initWithHCube:(__weak HCube *)hcube
                      baseURL:(NSURL *)baseURL;

/**
 It dispatch queue of events onto the server.

 @param eventsQueue Queue of HCEvent events
 */
- (void)dispatchEventsQueue:(NSMutableArray <HCEvent *>*)eventsQueue;


/**
 It dispatch queue of qualifies onto the server.

 @param qualifyQueue Queue of HCQualify events
 */
- (void)dispatchQualifyQueue:(NSMutableArray <HCQualify *>*)qualifyQueue;

@end

