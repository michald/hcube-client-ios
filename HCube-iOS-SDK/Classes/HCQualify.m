//
//  HCQualify.m
//

#import "HCQualify.h"
#import "HCube.h"
#import "HCubePrivate.h"
#import "HCQualifyPrivate.h"

@implementation HCQualify

#pragma mark - Lifecycle

- (instancetype)initWithHCube:(__weak HCube *)hcube {
    self = [super init];
    if (self) {
        self.qualifies = [NSMutableDictionary dictionary];
        self.cube = hcube;
    }
    return self;
}

#pragma mark - Public

- (void)setProperties:(NSDictionary *)properties {
    if (properties.count == 0) {
        return;
    }
    properties = [HCube verifyAndFilterPropertiesTypes:properties];
    [self.qualifies addEntriesFromDictionary:properties];
    [self addQualifyInQueue];
}

- (void)setProperty:(NSString *)property withObject:(id)object {
    if (property.length == 0 || !object) {
        return;
    }
    NSDictionary *propertyObject = @{property:object};
    [self setProperties:propertyObject];
}

- (void)clearQualify {
    self.qualifies = [NSMutableDictionary dictionary];
    [self addQualifyInQueue];
}

#pragma mark - Private

- (void)addQualifyInQueue {
    HCQualify *qualify = [self copy];
    [self.cube.qualifyQueue addObject:qualify];
}

#pragma mark - <NSCoding>

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.qualifies forKey:HCQualifyCodingPropertiesKey];
}
- (nullable instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        self.qualifies = [aDecoder decodeObjectForKey:HCQualifyCodingPropertiesKey];
    }
    return self;
}

#pragma mark - <NSCopying>

- (id)copyWithZone:(NSZone *)zone {
    HCQualify *copy = [[[self class] allocWithZone:zone] init];
    if (copy) {
        copy.qualifies = [self.qualifies mutableCopy];
        copy.cube = [self cube];
    }
    return copy;
}

#pragma mark - <HCSerializationProtocol>

- (NSDictionary *)serializedObject {
    NSMutableArray *qualifies = [NSMutableArray new];
    for (NSString *key in [self.qualifies allKeys]) {
        id value = self.qualifies[key];
        
        NSDictionary *serializedQualify = @{
            @"ukey" : key,
            @"uvalue" : value
        };
        [qualifies addObject:serializedQualify];
    }
    
    NSMutableDictionary *uniqueDetails = [NSMutableDictionary new];
    [uniqueDetails setObject:qualifies forKey:@"uniqueDetails"];
    return [uniqueDetails copy];
}

#pragma mark - <HCObjectEmptyProtocol>

- (BOOL)isEmpty {
    return self.qualifies.count == 0;
}

@end
