//
//  HCube+Persistence.h
//

#import "HCube.h"

@interface HCube (Persistence)

- (void)archiveAll;
- (void)archiveEvents;
- (void)archiveCommonProperties;
- (void)archiveQualifies;

- (void)unarchiveAll;
- (void)unarchiveEvents;
- (void)unarchiveCommonProperties;
- (void)unarchiveQualifies;


@end
