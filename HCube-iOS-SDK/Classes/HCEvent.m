//
//  HCEvent.m
//

#import "HCEvent.h"

NSString *const HCEventCodingEventNameKey = @"EventName";
NSString *const HCEventCodingPropertiesKey = @"EventProperties";

@implementation HCEvent

- (instancetype)initWithName:(NSString *)name
                  properties:(NSDictionary *)properties {
    self = [self init];
    if (self) {
        self.eventName = name;
        self.properties = properties;
    }
    return self;
}

#pragma mark - Private

- (NSString *)description {
    return [NSString stringWithFormat:@"<Event: %p with name: %@>", self, self.eventName];
}

#pragma mark - <NSCoding>

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.eventName forKey:HCEventCodingEventNameKey];
    [aCoder encodeObject:self.properties forKey:HCEventCodingPropertiesKey];
}
- (nullable instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        self.eventName = [aDecoder decodeObjectForKey:HCEventCodingEventNameKey];
        self.properties = [aDecoder decodeObjectForKey:HCEventCodingPropertiesKey];
    }
    return self;
}

#pragma mark - <HCSerializationProtocol>

- (NSDictionary *)serializedObject {
    NSMutableDictionary *serializedObject = [NSMutableDictionary new];
    serializedObject[@"state_name"] = self.eventName;
    [serializedObject addEntriesFromDictionary:self.properties];
    return [serializedObject copy];
}

#pragma mark - <HCObjectEmptyProtocol>

- (BOOL)isEmpty {
    return (self.eventName.length == 0);
}


@end
