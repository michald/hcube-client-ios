//
//  HCube+Persistence.m
//

#import "HCube+Persistence.h"
#import "HCubePrivate.h"
#import "HCLogger.h"

NSString *const HCubePersistenceCommonPropertiesKey = @"commonProperties";
NSString *const HCubePersistenceUniqueIdKey = @"uniqueId";

@implementation HCube (Persistence)

- (void)archiveAll {
    [self archiveCommonProperties];
    [self archiveEvents];
    [self archiveQualifies];
}
- (void)unarchiveAll {
    [self unarchiveCommonProperties];
    [self unarchiveEvents];
    [self unarchiveQualifies];
}

#pragma mark - URLs

- (NSURL *)eventsURL {
    return [[self class] URLToStore:@"events"];
}

- (NSURL *)commonPropertiesURL {
    return [[self class] URLToStore:@"commonProperties"];
}

- (NSURL *)qualifiesURL {
    return [[self class] URLToStore:@"qualifies"];
}

#pragma mark - Archivation

- (void)archiveEvents {
    NSURL *url = [self eventsURL];
    if (![[self class] archiveObject:[self eventsQueue] toURL:url]) {
        [HCLogger logWithLevel:HCLoggerLevelTypeError
        format:@"%@ events isn't archived", self];
    }
}

- (void)archiveCommonProperties {
    NSURL *url = [self commonPropertiesURL];
    NSMutableDictionary *dictionary = [NSMutableDictionary new];
    dictionary[HCubePersistenceCommonPropertiesKey] = self.commonProperties;
    dictionary[HCubePersistenceUniqueIdKey] = self.uniqueId;
    if (![[self class] archiveObject:dictionary toURL:url]) {
        [HCLogger logWithLevel:HCLoggerLevelTypeError
        format:@"%@ common properties isn't archived", self];
    }
}

- (void)archiveQualifies {
    NSURL *url = [self qualifiesURL];
    if (![[self class] archiveObject:[self qualifyQueue] toURL:url]) {
        [HCLogger logWithLevel:HCLoggerLevelTypeError
        format:@"%@ qualifies isn't archived", self];
    }
}

#pragma mark - Unarchivation

- (void)unarchiveEvents {
    NSURL *url = [self eventsURL];
    NSArray *unarchivedEvents = [[self class] unarchiveFromURL:url];
    self.eventsQueue = [NSMutableArray arrayWithArray:unarchivedEvents];
}

- (void)unarchiveCommonProperties {
    NSURL *url = [self commonPropertiesURL];
    NSDictionary *dictionary = [[self class] unarchiveFromURL:url];
    NSDictionary *commonProperties = dictionary[HCubePersistenceCommonPropertiesKey];
    self.commonProperties = [NSMutableDictionary dictionaryWithDictionary:commonProperties];
    self.uniqueId = dictionary[HCubePersistenceUniqueIdKey] ?: [self defaultUniqueId];
}

- (void)unarchiveQualifies {
    NSURL *url = [self qualifiesURL];
    NSArray *unarchivedQualifies = [[self class] unarchiveFromURL:url];
    self.qualifyQueue = [NSMutableArray arrayWithArray:unarchivedQualifies];
}

#pragma mark - Helpers

+ (NSURL *)URLToStore:(NSString *)store {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *filename = [NSString stringWithFormat:@"hcube_%@.plist", store];
    return [[[fileManager URLsForDirectory:NSLibraryDirectory
                                 inDomains:NSUserDomainMask] lastObject]
            URLByAppendingPathComponent:filename];
}

+ (BOOL)archiveObject:(id<NSCoding>)object toURL:(NSURL *)url {
    @try {
        if ([NSKeyedArchiver archiveRootObject:object toFile:[url path]]) {
            return YES;
        }
    } @catch (NSException *exception) {
        [HCLogger logWithLevel:HCLoggerLevelTypeError
        format:@"HCube caught exeption %@ with reason %@. \
         Please, check your values. \
         They should support NSCoding protocol",
         exception.name,
         exception.reason];
        abort();
    }
    return NO;
}

+ (id)unarchiveFromURL:(NSURL *)url {
    id unarchivedObject = nil;
    @try {
        unarchivedObject = [NSKeyedUnarchiver unarchiveObjectWithFile:[url path]];
    } @catch (NSException *exception) {
        [HCLogger logWithLevel:HCLoggerLevelTypeError
        format:@"HCube caught exeption %@ with reason %@.",
         exception.name,
         exception.reason];
        abort();
    }
    return unarchivedObject;
}

@end
