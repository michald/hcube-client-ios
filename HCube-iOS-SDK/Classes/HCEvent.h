//
//  HCEvent.h
//

#import <Foundation/Foundation.h>
#import "HCSerializationProtocol.h"
#import "HCObjectEmptyProtocol.h"

//TODO: Realize HCObjectEmptyProtocol;
@interface HCEvent : NSObject <NSCoding, HCSerializationProtocol, HCObjectEmptyProtocol>

@property (nonatomic, copy) NSString *eventName;
@property (nonatomic, copy) NSDictionary *properties;

- (instancetype)initWithName:(NSString *)name
                  properties:(NSDictionary *)properties;

@end
