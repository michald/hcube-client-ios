//
//  HCubePrivate.h
//

#import <SystemConfiguration/SystemConfiguration.h>
#import "HCube.h"
#import "HCConnection.h"
#import "HCLogger.h"
#import "HCQualify.h"
#import "HCLocator.h"
#import <CoreLocation/CoreLocation.h>

@interface HCube() {
    NSTimeInterval _dispatchInterval;
}

@property (nonatomic, copy) NSString *token;
@property (atomic, copy) NSString *uniqueId;
@property (nonatomic) dispatch_queue_t serialQueue;
@property (atomic, strong) NSMutableDictionary <NSString *, id>* commonProperties;
@property (atomic, strong) HCConnection *connection;
@property (atomic, strong) NSMutableArray *eventsQueue;
@property (atomic, strong) NSMutableArray *qualifyQueue;
@property (atomic, strong) NSTimer *dispatchTimer;
@property (atomic, strong) HCQualify *qualify;
@property (atomic, strong) CLLocation *lastLocation;

#pragma mark -

- (instancetype)initWithToken:(NSString *)token;

#pragma mark - 

- (void)dispatch;

#pragma mark - Helpers

- (NSString *)defaultUniqueId;

#pragma mark - Properties Checks

+ (NSDictionary *)verifyAndFilterPropertiesTypes:(NSDictionary *)properties;

@end
