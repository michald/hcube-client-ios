//
//  HCube.m
//

#import <UIKit/UIKit.h>

#import "HCube.h"
#import "HCubePrivate.h"
#import "HCLogger.h"
#import "HCube+Persistence.h"
#import "HCEvent.h"
#import "HCQualify.h"

#include <sys/sysctl.h>

#import <arpa/inet.h>
#import <ifaddrs.h>
#import <netdb.h>
#import <sys/socket.h>
#import <netinet/in.h>

NSString *const HCubeBaseURLString = @"https://input.dataphantom.com/v0/";

@implementation HCube

static HCube *sharedInstance = nil;

+ (HCube *)provideWithToken:(NSString *)token {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] initWithToken:token];
    });
    return sharedInstance;
}

+ (HCube *)sharedInstance {
    if (sharedInstance == nil) {
        [HCLogger logWithLevel:HCLoggerLevelTypeError
                        format:@"First, you should call provideWithToken: method"];
        abort();
    }
    return sharedInstance;
}

- (instancetype)initWithToken:(NSString *)token {
    self = [super init];
    if (self) {
        self.token = token;
        NSString *queueLabel = [NSString stringWithFormat:@"com.hcube.%@", self.token];
        self.serialQueue = dispatch_queue_create([queueLabel UTF8String], DISPATCH_QUEUE_SERIAL);
        self.commonProperties = [NSMutableDictionary new];

        NSURL *url = [NSURL URLWithString:HCubeBaseURLString];
        self.connection = [[HCConnection alloc] initWithHCube:self baseURL:url];
        self.eventsQueue = [NSMutableArray array];
        self.qualifyQueue = [NSMutableArray array];
        self.qualify = [[HCQualify alloc] initWithHCube:self];
        self.uniqueId = [self defaultUniqueId];

#if DEBUG
        self.dispatchInterval = 1;
#else
        self.dispatchInterval = 60;
#endif

        [self unarchiveAll];
    }
    return self;
}

#pragma mark - Custom Accessors

+ (void)setEnableLogging:(BOOL)enableLogging {
    [HCLogger setEnableLogging:enableLogging];
}

+ (BOOL)isLoggingEnabled {
    return [HCLogger isLoggingEnabled];
}

- (void)setEnableGeoLocation:(BOOL)enableGeoLocation {
    _enableGeoLocation = enableGeoLocation;
    if (enableGeoLocation) {
        [self updateLocation];
    }
    [HCLogger logWithLevel:HCLoggerLevelTypeInfo format:
     self.enableGeoLocation ? @"GeoLocation is enabled" : @"GeoLocation is disabled"];
}

#pragma mark - Helpers

- (NSString *)description {
    return [NSString stringWithFormat:@"<HCube: %p, token: %@>", self, self.token];
}

- (NSString *)defaultUniqueId {
    NSString *UUID = [[NSUUID UUID] UUIDString];
    return UUID;
}

- (NSString *)systemVersion {
    UIDevice *device = [UIDevice currentDevice];
    return [device systemVersion];
}

- (NSString *)deviceModel {
    size_t size;
    sysctlbyname("hw.model", NULL, &size, NULL, 0);

    char *answer = malloc(size);
    sysctlbyname("hw.model", answer, &size, NULL, 0);

    NSString *results = [NSString stringWithCString:answer encoding: NSUTF8StringEncoding];

    free(answer);
    return results;
}

- (NSString *)timestamp {
    NSDate *date = [NSDate date];
    return [[self dateFormatter] stringFromDate:date];
}

- (CGSize)realScreenSize {
    UIScreen *screen = [UIScreen mainScreen];
    CGSize realScreenSize = CGSizeZero;
    if ([screen respondsToSelector:@selector(nativeBounds)]) {
        realScreenSize = [screen nativeBounds].size;
    } else {
        CGRect screenBounds = [screen bounds];
        screenBounds.size.height *= [screen scale];
        screenBounds.size.width *= [screen scale];
        realScreenSize = screenBounds.size;
    }
    return realScreenSize;
}

- (NSString *)applicationVersion {
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
}

- (NSString *)applicationBuild {
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
}

- (NSDateFormatter *)dateFormatter {
    static NSDateFormatter *dateFormatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss";
        dateFormatter.timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    });
    return dateFormatter;
}

#if DEBUG

+ (void)assertPropertiesTypes:(NSDictionary *)properties {
    for (id key in properties) {
        id value = properties[key];
        NSAssert([key isKindOfClass:[NSString class]],
                 @"Keys should be a NSString, now it %@. { %@ : %@ }",
                 [key class], key, value);

        NSAssert([value isKindOfClass:[NSNumber class]] ||
                 [value isKindOfClass:[NSString class]] ||
                 [value isKindOfClass:[NSDate class]] ||
                 [value isKindOfClass:[NSURL class]],
                 @"Values should be NSNumber, NSString, NSDate, NSURL, now it %@. { %@ : %@ }",
                 [value class], key, value);
    }
}

#endif

+ (NSDictionary *)filterPropertiesTypes:(NSDictionary *)properties {
    NSMutableDictionary *dictionary = [NSMutableDictionary new];
    for (id key in properties) {
        id value = properties[key];

        if (![key isKindOfClass:[NSString class]]) {
            [HCLogger logWithLevel:HCLoggerLevelTypeWarning
                            format:
             @"Keys should be a NSString, now it %@. { %@ : %@ }",
             [key class], key, value];
            continue;
        }

        if (![value isKindOfClass:[NSNumber class]] &&
            ![value isKindOfClass:[NSString class]] &&
            ![value isKindOfClass:[NSDate class]] &&
            ![value isKindOfClass:[NSURL class]]) {
            [HCLogger logWithLevel:HCLoggerLevelTypeWarning
                            format:
             @"Values should be NSNumber, NSString, NSDate, NSURL, now it %@. { %@ : %@ }",
             [value class], key, value];
            continue;
        }
        [dictionary setObject:value forKey:key];
    }
    return [dictionary copy];
}

+ (NSDictionary *)verifyAndFilterPropertiesTypes:(NSDictionary *)properties {
#if DEBUG
    [self assertPropertiesTypes:properties];
#endif
    return [self filterPropertiesTypes:properties];
}

#pragma mark - Identification

- (void)identify:(NSString *)uniqueId {

    NSCharacterSet *lattersSet = [NSCharacterSet letterCharacterSet];
    NSRange range = [uniqueId rangeOfCharacterFromSet:lattersSet];

    if (range.location == NSNotFound) {
        uniqueId = [@"u__" stringByAppendingString:uniqueId];
    }

    if (uniqueId.length == 0) {
        [HCLogger logWithLevel:HCLoggerLevelTypeWarning
                        format:@"%@ unique Id cannot be empty"];
        return;
    }

    dispatch_async(self.serialQueue, ^{
        self.uniqueId = uniqueId;
        [self archiveCommonProperties];
    });
}

#pragma mark - Tracking

- (void)trackEvent:(NSString *)event {
    [self trackEvent:event properties:nil];
}

- (void)trackEvent:(nullable NSString *)event
        properties:(nullable NSDictionary <NSString *, id>*)properties; {

    if (event.length == 0) {
        [HCLogger logWithLevel:HCLoggerLevelTypeWarning
                        format:@"%@ event name cannot be empty", self];
        return;
    }

    properties = [[self class] verifyAndFilterPropertiesTypes:properties];

    dispatch_async(self.serialQueue, ^{

        NSMutableDictionary *allProperties = [NSMutableDictionary
                                              dictionaryWithDictionary:properties];

        [allProperties addEntriesFromDictionary:[self getCommonProperties]];
        [allProperties addEntriesFromDictionary:[self systemProperties]];
        [allProperties addEntriesFromDictionary:[self dynamicProperties]];
        [allProperties addEntriesFromDictionary:[self locationProperties]];

        HCEvent *eventObject = [[HCEvent alloc]
                                initWithName:event
                                properties:[allProperties copy]];

        if (self.eventsQueue.count > 1000) {
            [self.eventsQueue removeObjectAtIndex:0];
        }
        [self.eventsQueue addObject:eventObject];
        [self archiveEvents];
    });

    [self dispatch];
}

#pragma mark - Connection

- (NSTimeInterval)dispatchInterval {
    return _dispatchInterval;
}

- (void)setDispatchInterval:(NSTimeInterval)dispatchInterval {
    @synchronized (self) {
        _dispatchInterval = dispatchInterval;
    }
    [self startDispatchTimer];
}

- (void)startDispatchTimer {
    [self endDispatchTimer];
    dispatch_async(dispatch_get_main_queue(), ^{
        self.dispatchTimer = [NSTimer
                              scheduledTimerWithTimeInterval:self.dispatchInterval
                              target:self
                              selector:@selector(dispatch)
                              userInfo:nil
                              repeats:YES];
    });
}

- (void)endDispatchTimer {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.dispatchTimer) {
            [self.dispatchTimer invalidate];
        }
        self.dispatchTimer = nil;
    });
}

- (void)dispatch {
    [self updateLocation];
    dispatch_async(self.serialQueue, ^{
        [self.connection dispatchEventsQueue:self.eventsQueue];
        [self.connection dispatchQualifyQueue:self.qualifyQueue];
        [self archiveAll];
    });
}

#pragma mark - Properties

- (void)addCommonProperties:(NSDictionary <NSString *, id>*)properties {
    dispatch_async(self.serialQueue, ^{
        [self.commonProperties addEntriesFromDictionary:properties];
        [self archiveCommonProperties];
    });
}

- (void)addCommonPropertiesOnce:(NSDictionary <NSString *, id>*)properties {
    dispatch_async(self.serialQueue, ^{
        NSArray *allKeys = [self.commonProperties allKeys];
        for (NSString *key in properties) {
            if (![allKeys containsObject:key]) {
                id value = properties[key];
                self.commonProperties[key] = value;
            }
        }
        [self archiveCommonProperties];
    });

}

- (void)removeCommonPropertyByName:(NSString *)propertyName {
    dispatch_async(self.serialQueue, ^{
        [self.commonProperties removeObjectForKey:propertyName];
        [self archiveCommonProperties];
    });
}

- (void)clearAllCommonProperties {
    dispatch_async(self.serialQueue, ^{
        self.commonProperties = [NSMutableDictionary dictionary];
        [self archiveCommonProperties];
    });
}

- (NSDictionary <NSString *, id>*)getCommonProperties {
    return [self.commonProperties copy];
}

- (NSDictionary <NSString *, id>*)dynamicProperties {
    NSMutableDictionary *properties = [NSMutableDictionary dictionary];
    properties[@"unique_id"] = [self uniqueId];
    properties[@"timestamp"] = [self timestamp];
    return properties;
}

- (NSDictionary <NSString *, id>*)locationProperties {
    NSMutableDictionary *properties = [NSMutableDictionary dictionary];
    CLLocation *location = [self lastLocation];
    if (location) {
        properties[@"hcube_geo_location_latitude"]              = @(location.coordinate.latitude);
        properties[@"hcube_geo_location_longitude"]             = @(location.coordinate.longitude);
        properties[@"hcube_geo_location_speed"]                 = @(location.speed);
        properties[@"hcube_geo_location_horizontal_accuracy"]   = @(location.horizontalAccuracy);
        properties[@"hcube_geo_location_vertical_latitude"]     = @(location.verticalAccuracy);
        properties[@"hcube_geo_location_altitude"]              = @(location.altitude);
    }
    return properties;
}

- (NSDictionary <NSString *, id>*)systemProperties {
    static NSDictionary *systemProperties = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSMutableDictionary *properties = [NSMutableDictionary dictionary];

        properties[@"hcube_device_model"] = @([self deviceModel].intValue);
        properties[@"hcube_device_os_version"] = @([self systemVersion].intValue);
//        properties[@"hcube_library"] = @"iOS-SDK-OBJC";
//        properties[@"hcube_library_version"] = @"";
        properties[@"hcube_device_manufacturer"] = @"Apple";
        properties[@"hcube_app_version"] = @([self applicationVersion].intValue);
        properties[@"hcube_app_build"] = @([self applicationBuild].intValue);

        CGSize screenSize = [self realScreenSize];
        properties[@"hcube_screen_height"] = @(screenSize.height);
        properties[@"hcube_screen_width"] = @(screenSize.width);

        systemProperties = [properties copy];
    });
    return systemProperties;
}

#pragma mark - Location

- (void)updateLocation {
    if (_enableGeoLocation) {
        CLLocationAccuracy mostAccuracy = kCLLocationAccuracyHundredMeters;
        [[HCLocator sharedInstance]
         requestLocationWithDesiredAccuracy:kCLLocationAccuracyHundredMeters
         block:^(CLLocation *location, CLLocationAccuracy accuracy) {
             if (accuracy < mostAccuracy) {
                 self.lastLocation = location;
             }
             [HCLogger logWithLevel:HCLoggerLevelTypeInfo
                             format:@"Your location is %@", location];
         }];
    }
}

@end
