//
//  HCLocationRequest.h
//  Pods
//
//  Created by Dmitriy Karachentsov on 8/12/16.
//
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "HCLocator.h"

@interface HCLocationRequest : NSObject

@property (nonatomic, assign) CLLocationAccuracy accuracy;

@property (nonatomic, copy) HCLocationRequestBlock block;

@end
