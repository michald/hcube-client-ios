//
//  HCQualifyPrivate.h
//  Pods
//
//  Created by Dmitriy Karachentsov on 11/10/16.
//
//

#import "HCQualify.h"
#import "HCube.h"
#import "HCubePrivate.h"

NSString *const HCQualifyCodingPropertiesKey = @"QualifyProperties";

@interface HCQualify ()
@property (atomic, strong) NSMutableDictionary *qualifies;
@property (atomic, weak) HCube *cube;

- (void)addQualifyInQueue;

@end
