//
//  HCLocator.m
//  Pods
//
//  Created by Dmitriy Karachentsov on 8/12/16.
//
//

#import "HCLocator.h"
#import "HCLocationRequest.h"

static NSTimeInterval HCLocatorTimeoutInterval = 10.0;

@interface HCLocator() <CLLocationManagerDelegate>

/**
 <#Description#>
 */
@property (nonatomic, strong) CLLocationManager *locationManager;

/**
 <#Description#>
 */
@property (nonatomic, strong) CLLocation *currentLocation;

/**
 <#Description#>
 */
@property (nonatomic, strong) NSMutableArray<HCLocationRequest *> *locationRequests;

/**
 <#Description#>
 */
@property (nonatomic, assign) BOOL isUpdatingLocation;

/**
 <#Description#>
 */
@property (nonatomic, strong) NSTimer *timeoutTimer;

@end

@implementation HCLocator

static id _sharedInstance;

/**
 <#Description#>

 @return <#return value description#>
 */
+ (CLAuthorizationStatus)authorizationStatus {
    return [CLLocationManager authorizationStatus];
}

/**
 <#Description#>

 @return <#return value description#>
 */
+ (instancetype)sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [HCLocator new];
    });
    return _sharedInstance;
}

- (instancetype)init {
    NSAssert(_sharedInstance == nil, @"You should create HCLocator object using + [HCLocator sharedInstance] method");
    self = [super init];
    if (self) {
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
        _locationRequests = [NSMutableArray array];
    }
    return self;
}

#pragma mark - Private

- (void)startUpdatingLocation {
    
    [self requestAuthorizationIfNeeded];
    self.isUpdatingLocation = YES;
    [self.locationManager startUpdatingLocation];
}

- (void)stopUpdatingLocation {
    
    [self.locationManager stopUpdatingLocation];
    self.isUpdatingLocation = NO;
    self.locationRequests = nil;
}

- (void)startTimeoutTimer {
    [self cancelTimeoutTimer];
    
    self.timeoutTimer = [NSTimer
        scheduledTimerWithTimeInterval:HCLocatorTimeoutInterval
        target:self
        selector:@selector(timerFire:)
        userInfo:nil
        repeats:NO];
}

- (void)cancelTimeoutTimer {
    [self.timeoutTimer invalidate];
    self.timeoutTimer = nil;
}

- (void)timerFire:(NSTimer *)timer {
    [self cancelTimeoutTimer];
    
    [self processLocationRequests];
}

- (void)requestLocationWithDesiredAccuracy:(CLLocationAccuracy)accuracy block:(HCLocationRequestBlock)block {
    
    HCLocationRequest *request = [HCLocationRequest new];
    request.block = block;
    request.accuracy = accuracy;
    
    BOOL deferTimeout = ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined);
    if (!deferTimeout) {
        [self startTimeoutTimer];
    }
    
    [self addLocationRequest:request];
    
}

- (void)cancelAllLocationRequests {
    
    for (HCLocationRequest *request in self.locationRequests) {
        [self completeLocationRequest:request];
    }
    
}

- (void)requestAuthorizationIfNeeded {
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined) {
        BOOL hasAlwaysKey = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationAlwaysUsageDescription"] != nil;
        BOOL hasWhenInUseKey = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationWhenInUseUsageDescription"] != nil;
        if (hasAlwaysKey) {
            [self.locationManager requestAlwaysAuthorization];
        } else if (hasWhenInUseKey) {
            [self.locationManager requestWhenInUseAuthorization];
        } else {
            NSAssert(hasAlwaysKey || hasWhenInUseKey, @"You should provide a value for NSLocationWhenInUseUsageDescription or NSLocationAlwaysUsageDescription.");
        }
    }
}

- (void)processLocationRequests {
    CLLocation *location = [self currentLocation];
    
    if (location == nil) {
        return;
    }
        
    for (HCLocationRequest *request in self.locationRequests) {
        
        if (location.horizontalAccuracy > request.accuracy) {
            continue;
        }
        
        [self completeLocationRequest:request];
    }
    
    if (self.locationRequests.count == 0) {
        
        [self stopUpdatingLocation];
        
    }
    
}

- (void)completeAllLocationRequests {
    
    for (HCLocationRequest *request in self.locationRequests) {
        [self completeLocationRequest:request];
    }
    
}

- (void)completeLocationRequest:(HCLocationRequest *)request {
    if (request == nil) {
        return;
    }
    
    [self removeLocationRequest:request];
    
    CLLocation *currentLocation = self.currentLocation;
    CLLocationAccuracy accuracy = currentLocation.horizontalAccuracy;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (request.block) {
            request.block(currentLocation, accuracy);
        }
    });
}

- (void)addLocationRequest:(HCLocationRequest *)request {
    
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    if (status == kCLAuthorizationStatusDenied || status == kCLAuthorizationStatusRestricted) {
        [self completeLocationRequest:request];
    }
    
    self.locationManager.desiredAccuracy = [request accuracy];
    [self.locationRequests addObject:request];
   
    if ( ! self.isUpdatingLocation) {
        
        [self startUpdatingLocation];
        
    }
}

- (void)removeLocationRequest:(HCLocationRequest *)request {
    
    NSMutableArray *locations = [self locationRequests];
    [locations removeObject:request];
    
    self.locationRequests = locations;
}

#pragma mark - <CLLocationManagerDelegate>

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    CLLocation *lastLocation = [locations lastObject];
    self.currentLocation = lastLocation;
    
    [self processLocationRequests];
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if (status == kCLAuthorizationStatusDenied || status == kCLAuthorizationStatusRestricted) {
        
        [self completeAllLocationRequests];
        
    } else if (status == kCLAuthorizationStatusAuthorizedAlways || status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        
        [self startTimeoutTimer];
    }
    
}

@end


