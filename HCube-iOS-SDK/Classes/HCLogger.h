//
//  HCLogger.h
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, HCLoggerLevelType) {
    HCLoggerLevelTypeError = 1,
    HCLoggerLevelTypeWarning,
    HCLoggerLevelTypeInfo,
    HCLoggerLevelTypeDebug,
};

@interface HCLogger : NSObject

/**
 Property for enabling debug logs.
 There are following levels:
 
 + DEBUG   - Log all debug, informative, warning and error messages
 + INFO    - Log informative, warning and error messages
 + WARNING - Log warnings and errors
 + ERROR   - Log all errors
 
 Default is NO
 */
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
@property (class, assign, getter=isLoggingEnabled) BOOL enableLogging;
#else
+ (BOOL)isLoggingEnabled;
+ (void)setEnableLogging:(BOOL)enableLogging;
#endif

/**
 Logging messages with level

 @param level  Logging level DEBUG,INFO,WARNING or ERROR
 @param format Logging message
 */
+ (void)logWithLevel:(HCLoggerLevelType)level
              format:(NSString *)format, ...;

@end
