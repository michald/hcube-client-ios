//
//  HCube.h
//

#import <Foundation/Foundation.h>
#import "HCQualify.h"

@interface HCube : NSObject

/**
 Property for enabling debug logs.
 There are following levels:
 
 + DEBUG   - Log all debug, informative, warning and error messages
 + INFO    - Log informative, warning and error messages
 + WARNING - Log warnings and errors
 + ERROR   - Log all errors
 
 Default is NO
 */
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
@property (class, getter=isLoggingEnabled) BOOL enableLogging;
#else
+ (BOOL)isLoggingEnabled;
+ (void)setEnableLogging:(BOOL)enableLogging;
#endif

/**
 Switch for enable GeoLocation using CoreLocation
 
 Default is no
 */
@property (atomic, assign) BOOL enableGeoLocation;

/**
 Dispatching interval in seconds
 */
@property (atomic, assign) NSTimeInterval dispatchInterval;

/**
 Unique user identifier
 
 You can change it in -identify: method.
 */
@property (atomic, readonly) NSString * _Nonnull uniqueId;

/**
 Qualify entity
 
 With qualify you can add properties to each of your users.
 */
@property (atomic, readonly, strong) HCQualify * _Nonnull qualify;

/**
 This method creates HCube instance and returns it

 @param token Your project token, you can get it on your panel

 @return HCube instance
 */
+ (nullable HCube *)provideWithToken:(nullable NSString *)token;

/**
 This method returned HCube instance if you created it previously

 @return HCube instance
 */
+ (nullable HCube *)sharedInstance;

#pragma mark - Identification

/**
 With `-identify:` you can change user's identifier.

 @param uniqueId New identifier
 */
- (void)identify:(nullable NSString *)uniqueId;

#pragma mark - Tracking

/**
 Track an event with the event name.

 @param event Event properties.
 */
- (void)trackEvent:(nullable NSString *)event;

/**
 Track an event with the event name and properties.

 @param event      Event name.
 @param properties Event properties.
 */
- (void)trackEvent:(nullable NSString *)event
        properties:(nullable NSDictionary <NSString *, id>*)properties;

#pragma mark - Common Properties

/**
 Set common properties

 @param properties Your properties
 */
- (void)addCommonProperties:(nullable NSDictionary <NSString *, id>*)properties;

/**
 Set common properties once (without rewriting).

 @param properties Your properties
 */
- (void)addCommonPropertiesOnce:(nullable NSDictionary <NSString *, id>*)properties;

/**
 Remove common property by name.

 @param propertyName Property name.
 */
- (void)removeCommonPropertyByName:(nullable NSString *)propertyName;

/**
 Remove all of properties.
 */
- (void)clearAllCommonProperties;

/**
 Get all of your common properties;

 @return All of common properties
 */
- (nonnull NSDictionary <NSString *, id>*)getCommonProperties;

@end
