//
//  HCConnection.m
//

#import "HCConnection.h"
#import "HCConnectionPrivate.h"
#import "HCEncoders.h"
#import "HCLogger.h"
#import "HCEvent.h"
#import "HCQualify.h"

#import "HCube.h"
#import "HCubePrivate.h"

NSString *const HCConnectionEventsEndpoint = @"input/event/send";
NSString *const HCConnectionQualifyEndpoint = @"input/qualify";

@implementation HCConnection

- (instancetype)initWithHCube:(__weak HCube *)hcube baseURL:(NSURL *)baseURL {
    self = [super init];
    if (self) {
        self.cube = hcube;
        self.baseURL = baseURL;
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
#if DEBUG
        self.session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
#else
        self.session = [NSURLSession sessionWithConfiguration:configuration];
#endif
        
        [self setUpReachability];
    }
    return self;
}

- (void)dispatchEventsQueue:(NSMutableArray *)eventsQueue {
    [self dispatchQueue:eventsQueue withEndpoint:HCConnectionEventsEndpoint];
}

- (void)dispatchQualifyQueue:(NSMutableArray *)qualifyQueue {
    [self dispatchQueue:qualifyQueue withEndpoint:HCConnectionQualifyEndpoint];
}

- (void)dispatchQueue:(NSMutableArray *)queue withEndpoint:(NSString *)endpoint {
    if (![self isNetworkReachable]) {
        return;
    }
    
    while (queue.count > 0) {
        
        NSObject<HCSerializationProtocol, HCObjectEmptyProtocol> *object = [queue firstObject];
        
        if ([object isEmpty]) {
            [queue removeObject:object];
            continue;
        }
        
        NSURLRequest *URLRequest = [self buildRequestWithObject:object endpoint:endpoint];
        
        dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
        
        __block BOOL completedWithError = NO;
        
        [[self.session dataTaskWithRequest:URLRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            
            BOOL success = [self handleResponse:response withError:error];
            
            if (!success) {
                if (error) {
                    [HCLogger logWithLevel:HCLoggerLevelTypeError
                                    format:@"HCube caught error %@ when trying to send event %@",
                     error,
                     object];
                } else {
                    [HCLogger logWithLevel:HCLoggerLevelTypeError
                                    format:@"Event %@ isn't sent!", object];
                }
                completedWithError = YES;
            } else {
                [HCLogger logWithLevel:HCLoggerLevelTypeDebug
                                format:@"Event %@ sent with success!", object];
            }
            
            dispatch_semaphore_signal(semaphore);
        }] resume];
        
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
        
        if (completedWithError) {
            break;
        }
        
        [queue removeObject:object];
    }
}

- (NSURLRequest *)buildRequestWithObject:(id<HCSerializationProtocol>)object endpoint:(NSString *)endpoint {
    
    NSString *path = [self URLPathByEndpoint:endpoint];
    
    NSDictionary *properties = [object serializedObject];
    NSString *encodedJSON = [[properties JSONData] base64];
    path = [path stringByAppendingPathComponent:encodedJSON];
    
    NSURLComponents *components = [[NSURLComponents alloc] initWithURL:self.baseURL resolvingAgainstBaseURL:YES];
    components.path = path;
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[components URL]];
    [request setHTTPMethod:@"GET"];
    [request setValue:self.cube.token forHTTPHeaderField:@"token"];
    
    return [request copy];
}
    
- (NSString *)URLPathByEndpoint:(NSString *)endpoint {
    
    NSString *path = [self.baseURL path];
    path = [path stringByAppendingPathComponent:endpoint];
    
    if ([endpoint isEqualToString:HCConnectionQualifyEndpoint]) {
        path = [path stringByAppendingPathComponent:self.cube.uniqueId];
    }
    
    return path;
}

- (BOOL)checkResponse:(NSHTTPURLResponse *)response withError:(NSError *)error {
    return (error == nil) && (response.statusCode == 200);
}

- (BOOL)handleResponse:(NSHTTPURLResponse *)response withError:(NSError *)error  {
    [HCLogger logWithLevel:HCLoggerLevelTypeDebug
    format:@"Header Fields: %@", response.allHeaderFields];
    [HCLogger logWithLevel:HCLoggerLevelTypeDebug
    format:@"Error Description: %@", [error localizedDescription]];
    [HCLogger logWithLevel:HCLoggerLevelTypeDebug
    format:@"Status Code: %d", response.statusCode];
    [HCLogger logWithLevel:HCLoggerLevelTypeDebug
    format:@"Status Code Description: %@", [NSHTTPURLResponse localizedStringForStatusCode:response.statusCode]];
    
    return [self checkResponse:response withError:error];
}

#pragma mark - Reachability

- (void)setUpReachability {
    _reachabilityRef = SCNetworkReachabilityCreateWithName(NULL, "google.com");
    if (_reachabilityRef != NULL) {
        SCNetworkReachabilityContext context = {0, (__bridge void*)self, NULL, NULL, NULL};
        SCNetworkReachabilitySetCallback(_reachabilityRef, HCReachabilityCallback, &context);
        SCNetworkReachabilityScheduleWithRunLoop(_reachabilityRef, CFRunLoopGetCurrent(), kCFRunLoopDefaultMode);
    } else {
        CFRelease(_reachabilityRef);
    }
}

static void HCReachabilityCallback(SCNetworkReachabilityRef target, SCNetworkReachabilityFlags flags, void* info) {
    HCConnection *connection = (__bridge HCConnection *)info;
    if (connection && [connection isKindOfClass:[HCConnection class]]) {
        [connection handleReachabilityUpdate:flags];
    }
}

- (void)handleReachabilityUpdate:(SCNetworkReachabilityFlags)flags {
    self.networkReachable = (flags & kSCNetworkReachabilityFlagsReachable) != 0;
}

#if DEBUG

#pragma mark - Allow use untrusted cert in debug mode

- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge
 completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential * _Nullable credential))completionHandler {
    if([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]){
        if ([self.baseURL.absoluteString containsString:challenge.protectionSpace.host]) {
            NSURLCredential *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
            completionHandler(NSURLSessionAuthChallengeUseCredential,credential);
        }
    }
}
#endif

#pragma mark - Dealloc

- (void)dealloc {
    if (_reachabilityRef != NULL) {
        SCNetworkReachabilityUnscheduleFromRunLoop(_reachabilityRef, CFRunLoopGetCurrent(), kCFRunLoopDefaultMode);
        CFRelease(_reachabilityRef);
    }
}

@end
