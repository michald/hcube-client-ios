//
//  HCSerializationProtocol.h
//

@protocol HCSerializationProtocol <NSObject>

/**
 Serialization protocol

 @return should return serialized object
 */
- (NSDictionary *)serializedObject;

@end
