//
//  HCQualify.h
//

#import <Foundation/Foundation.h>
#import "HCSerializationProtocol.h"
#import "HCObjectEmptyProtocol.h"

@class HCube;

@interface HCQualify : NSObject <NSCoding, NSCopying, HCSerializationProtocol, HCObjectEmptyProtocol>

/**
 Initializes and returns a newly created HCQualify instance

 @param hcube HCube instance

 @return HCQualify instance
 */
- (instancetype)initWithHCube:(__weak HCube *)hcube;

/**
 Set qualify properties

 @param properties Dictionary of properties
 */
- (void)setProperties:(NSDictionary *)properties;

/**
 Set one property

 @param property Property name
 @param object   Property object
 */
- (void)setProperty:(NSString *)property withObject:(id)object;

/**
 Clear all of qualify properties.
 */
- (void)clearQualify;

@end
