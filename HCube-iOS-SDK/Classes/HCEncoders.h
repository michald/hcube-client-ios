//
//  HCHelpers.h
//

#import <Foundation/Foundation.h>

@interface NSObject (Encoding)

- (nullable NSData *)JSONData;

@end

@interface NSData (Encoding)

- (nullable NSString *)base64;

@end
