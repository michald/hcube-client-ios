//
//  HCConnectionPrivate.h
//  Pods
//
//  Created by Dmitriy Karachentsov on 14/10/16.
//
//

#import "HCConnection.h"
#import <SystemConfiguration/SystemConfiguration.h>

extern NSString *const HCConnectionEventsEndpoint;
extern NSString *const HCConnectionQualifyEndpoint;

@protocol HCSerializationProtocol;

@interface HCConnection()
@property (nonatomic, strong) NSURL *baseURL;
@property (nonatomic, strong) NSURLSession *session;
@property (atomic, weak) HCube *cube;

@property (nonatomic, assign) SCNetworkReachabilityRef reachabilityRef;
@property (nonatomic, assign) BOOL networkReachable;

- (NSURLRequest *)buildRequestWithObject:(id<HCSerializationProtocol>)object
                                endpoint:(NSString *)endpoint;
- (NSString *)URLPathByEndpoint:(NSString *)endpoint;
- (BOOL)checkResponse:(NSHTTPURLResponse *)response withError:(NSError *)error;
- (void)dispatchQueue:(NSMutableArray *)queue withEndpoint:(NSString *)endpoint;

@end

