//
//  HCObjectEmptyProtocol.h
//

#import <Foundation/Foundation.h>

@protocol HCObjectEmptyProtocol <NSObject>

/**
 Empty Protocol

 @return should return YES if object is empty, otherwise it return NO.
 */
- (BOOL)isEmpty;


@end

