//
//  HCHelpers.m
//

#import "HCEncoders.h"
#import "HCLogger.h"

@implementation NSObject (Encoding)

- (nullable NSData *)JSONData {
    NSData *data = nil;
    if ([NSJSONSerialization isValidJSONObject:self]) {
        data = [NSJSONSerialization
                dataWithJSONObject:self
                options:0
                error:nil];
    }
    return data;
}

@end

@implementation NSData (Encoding)

- (NSString *)base64 {
    if (self.length == 0) {
        return nil;
    }
    return [self base64EncodedStringWithOptions:0];
}

@end
