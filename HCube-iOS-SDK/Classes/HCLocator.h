//
//  HCLocator.h
//  Pods
//
//  Created by Dmitriy Karachentsov on 8/12/16.
//
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

typedef void (^HCLocationRequestBlock)(CLLocation *location, CLLocationAccuracy accuracy);

@interface HCLocator : NSObject

+ (CLAuthorizationStatus)authorizationStatus;

+ (instancetype)sharedInstance;

- (void)requestLocationWithDesiredAccuracy:(CLLocationAccuracy)accuracy
                                     block:(HCLocationRequestBlock)block;

- (void)cancelAllLocationRequests;

@end
