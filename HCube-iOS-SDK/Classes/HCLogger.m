//
//  HCLogger.m
//

#import "HCLogger.h"

NSString *const HCLoggerLevelError = @"ERROR";
NSString *const HCLoggerLevelWarning = @"WARNING";
NSString *const HCLoggerLevelInfo = @"INFO";
NSString *const HCLoggerLevelDebug = @"DEBUG";

static BOOL _enableLogging = NO;

@implementation HCLogger

+ (void)logWithLevel:(HCLoggerLevelType)level format:(NSString *)format, ... {
    if (![self isEnableLogging]) {
        return;
    }
    va_list arg_list;
    va_start(arg_list, format);
    NSString *formattedString = [[NSString alloc] initWithFormat:format arguments:arg_list];
    NSLog(@"<%@>: %@", stringifyLevel(level), formattedString);
    va_end(arg_list);
}

+ (void)setEnableLogging:(BOOL)enableLogging {
    _enableLogging = enableLogging;
}

+ (BOOL)isEnableLogging {
    return _enableLogging;
}

#pragma mark - Private

NSString *stringifyLevel(HCLoggerLevelType level) {
    NSString *stringLevel = nil;
    switch (level) {
        case HCLoggerLevelTypeError:
            stringLevel = HCLoggerLevelError;
            break;
        case HCLoggerLevelTypeWarning:
            stringLevel = HCLoggerLevelWarning;
            break;
        case HCLoggerLevelTypeInfo:
            stringLevel = HCLoggerLevelInfo;
            break;
        case HCLoggerLevelTypeDebug:
            stringLevel = HCLoggerLevelDebug;
            break;
    }
    return stringLevel;
}

@end
