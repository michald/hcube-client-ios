//
//  HCube-iOS-SDK.h
//  HCube-iOS-SDK
//
//  Created by Dmitriy Karachentsov on 27/1/17.
//  Copyright © 2017 Dmitriy Karachentsov. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for HCube-iOS-SDK.
FOUNDATION_EXPORT double HCube_iOS_SDKVersionNumber;

//! Project version string for HCube-iOS-SDK.
FOUNDATION_EXPORT const unsigned char HCube_iOS_SDKVersionString[];

#import "HCube.h"


