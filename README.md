# DataPhantom-iOS-SDK

[![Version](https://img.shields.io/cocoapods/v/HCube-iOS-SDK.svg?style=flat)](http://cocoapods.org/pods/HCube-iOS-SDK)
[![License](https://img.shields.io/cocoapods/l/HCube-iOS-SDK.svg?style=flat)](http://cocoapods.org/pods/HCube-iOS-SDK)
[![Platform](https://img.shields.io/cocoapods/p/HCube-iOS-SDK.svg?style=flat)](http://cocoapods.org/pods/HCube-iOS-SDK)

# Installation
Adding the DataPhantom iOS SDK (codename: hcube) to your project is very simple. Choose whichever method you're most comfortable with and follow the instructions below.

## Using Carthage

1. Add the following line to your `Cartfile`
````ruby
git "git@bitbucket.org:michald/hcube-client-ios.git"
````
2. Run `carthage update` in your project directory.
3. Drag `HCube.framework` from the appropriate platform directory in `Carthage/Build/` to the “Linked Frameworks and Libraries” section of your Xcode project’s “General” settings.
3. Add `#import <HCube/HCube.h>`

## Using CocoaPods
One of the easiest ways to integrate HCube-iOS-SDK into your iOS project is to use CocoaPods:

1. Add the following line to your `Podfile` `pod "HCube-iOS-SDK"`
2. In your project directory, run `pod update`
3. Add `#import <HCube-iOS-SDK/HCube.h>`

## Getting Started
To get started, import the `HCube.h` header file in your project's pch file. This will allow a global include of all the required headers.

If you're using CocoaPods or Carthage, your import should look like:
````objective-c
// Objective-
#import <HCube-iOS-SDK/HCube.h>
````
Otherwise, if you've added MagicalRecord's source files directly to your Objective-C project, your import should be:

````objective-c
#import "HCube-iOS-SDK/HCube.h"
````

Next, somewhere in your app delegate, exactly in the - applicationDidFinishLaunching: withOptions: method you should provide `token` using HCube's class method

````objective-c
+ (HCube *)provideWithToken:(NSString *)token;
````

To get `token` for your application, you should [register](https://test.turiia.com/#/sign_up) your account. Token will available at `Account Settings` in left side menu. Then put your `token` into function `provideWithToken:`

````objective-c
[HCube provideWithToken:"1fbm9s3hqevsacvrdbh0i5e63m"];
````

Method `provideWithToken:` creates global `HCube.sharedInstance` and then you can use it anywhere in your code to create additional properties or track events etc.

````objective-c
+ (HCube *)sharedInstance;
````


## Track
> Main event collection method that is used to collect events of various kinds from your app.

After library initialization you can send data by tracking your events.


To track events with all attached common properties, you can use `-trackEvent:`
````objective-c
HCube *hcube = [HCube sharedInstance];
[hcube trackEvent:@"Personal info"];
````
Otherwise, if you need to send some additional properties with event, you can track an event by calling `-trackEvent:properties:` with the event name and properties.
````objective-c
HCube *hcube = [HCube sharedInstance];
[hcube trackEvent:@"Personal info"
       properties:@{@"FirstName": @"John"}];
````

## Identify
> Allows you to identify a user uniquely.


By default, DataPhantom generates an ID for each user.
It is a random, unique string like so - "d1c221f2-5a37-4ec4-a1bc-6e0bded47c7c";
We store this ID in local storage.

All events sent via our sdks will contain this unique_id property.
But in several cases, you may wish to identify your visitor (say once they login to your app , or decide to signup after interacting anonymously first),
you can use this method and assign a value to this unique_id which may be used in your app o

````objective-c
[hcube identify:@"12345-67890-12345-67890"];

````

## Qualify

> Allows you to qualify a user with properties which can then be attributed to events/actions the user performs.

Upon signup, for example, if there are bunch of user properties that are available for the signed up user, these can be set using the `hcube.qualify` method.

These values just need to be sent once per user (assuming you don't have properties like credits which can keep on changing over time) and they are automatically attributed each event as it happens for the user.

!!! Note
"Attribution as it happens" is important as it allows us to capture the context of the event accurately.

Say for instance, there was a user property called credit, like so: `credit: 100` and it's value decreased as someone played more levels in a game. The behavior of a user when they have lots of credits may be different from a users who may not have lot of credits.


````objective-c
// 1) Identify your user.
[hcube identify:@"12345-67890-12345-67890"];

// 3)
[hcube.qualify setProperties:@{@"FirstName": @"John"}];
````

## Common properties

There may be times, where a bunch of events may share some properties (for instance user going through a wizard flow, and the properties of the form wizard).

### Getting

You can get all of properties using `-getCommonProperties:` method

````objective-c
[hcube getCommonProperties];
````

### Setting

If you want to store common properties call `-addCommonProperties:` or `-addCommonPropertiesOnce: `
````objective-c
[hcube addCommonProperties:@{@"FirstName": @"John"}];
````

If you want to set common properties once (without rewriting), you should use `-addCommonPropertiesOnce:` method
````objective-c
[hcube addCommonPropertiesOnce:@{@"FirstName": @"John"}];
````

### Removing

You can remove property by the name.
````objective-c
[hcube removeCommonPropertyByName:@"FirstName"];
````

### Updating

If you want to update value for property by name, you should use `removeCommonPropertyByName:` and after that use `addCommonProperties:` or `addCommonPropertiesOnce:`

### Clearing

To remove all properties, call `-clearAllCommonProperties`
````objective-c
[hcube clearAllCommonProperties];
````



# Logging
HCube-iOS-SDK has its own logger.
Logging can be configured by changing that property
````objective-c
@interface HCube : NSObject
@property (class, getter=isLoggingEnabled) BOOL enableLogging;
@end
````

# Logout

When a user signs out or logs out from you application, you should erase all properties from HCube.


# License

DataPhantom-iOS-SDK is available under the MIT license. See the LICENSE file for more info.
