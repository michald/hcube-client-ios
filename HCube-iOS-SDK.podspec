#
# Be sure to run `pod lib lint HCube-iOS-SDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'HCube-iOS-SDK'
  s.version          = '1.0.1'
  s.summary          = 'Tracking library for HCube.'
  s.homepage         = 'https://bitbucket.org/michald/hcube-client-ios'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Dmitriy Karachentsov' => 'd.karachentsov@gmail.com' }
  s.source           = { :git => 'https://bitbucket.org/michald/hcube-client-ios.git', :tag => s.version.to_s }
  s.ios.deployment_target = '8.0'
  s.source_files = 'HCube-iOS-SDK/Classes/*.{h,m}'
  s.private_header_files = 'HCube-iOS-SDK/Classes/HCubePrivate.h', 'HCube-iOS-SDK/Classes/HCQualifyPrivate.h', 'HCube-iOS-SDK/Classes/HCConnectionPrivate.h'
  s.frameworks = 'UIKit', 'Foundation', 'SystemConfiguration', 'CoreLocation'
end
